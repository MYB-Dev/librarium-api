const mongoose = require('mongoose');

const MemoirSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Please add a name'],
    unique: true,
    trim: true,
    maxlength: [50, 'Name cannot be more than 50 characters'],
  },
  slug: String,
  author: {
    type: [String],
    required: [true, 'Please add the Author'],
    unique: true,
    trim: true,
    maxlength: [50, 'Author name cannot be more than 50 characters'],
  },
  Supervisor: {
    type: [String],
    required: [true, 'Please add the Supervisor'],
    unique: true,
    trim: true,
    maxlength: [50, 'Author name cannot be more than 50 characters'],
  },
  type: {
    type: [String],
    required: [true, 'Please add the type'],
    trim: true,
    maxlength: [50, 'isbn cannot be more than 50 characters'],
    enum: [
      'Computer Sciences',
      'Natural sciences',
      'Material Scineces',
      'Mathematics',
      'Others',
    ],
  },
  quantity: {
    type: Number,
    required: [true, 'Please add the quantity'],
    min: [1, 'quantity must be atlest One'],
  },
  available: {
    type: Boolean,
    default: true,
  },
  addedat: {
    type: Date,
    default: Date.now,
  },
});
module.exports = mongoose.model('Memoir', MemoirSchema);
