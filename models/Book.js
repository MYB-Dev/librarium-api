const mongoose = require('mongoose');
const slugify = require('slugify');

const BookSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Please add a name'],
    unique: true,
    trim: true,
    maxlength: [100, 'Name cannot be more than 50 characters'],
  },
  slug: String,
  author: {
    type: [String],
    required: [true, 'Please add the Author'],
    trim: true,
    maxlength: [50, 'Author name cannot be more than 50 characters'],
  },
  isbn: {
    type: String,
    required: [true, 'Please add the Isbn'],
    unique: true,
    trim: true,
    maxlength: [50, 'isbn cannot be more than 50 characters'],
  },
  discription: {
    type: String,
    maxlength: [250, 'Discription cannot be more than 50 characters'],
  },
  type: {
    type: [String],
    required: [true, 'Please add the type'],
    trim: true,
    maxlength: [50, 'isbn cannot be more than 50 characters'],
    enum: [
      'Computer Sciences',
      'Natural sciences',
      'Material Scineces',
      'Mathematics',
      'Others',
    ],
  },
  quantity: {
    type: Number,
    required: [true, 'Please add the quantity'],
    min: [1, 'quantity must be atlest One'],
  },
  available: {
    type: Boolean,
    default: true,
  },
  addedAt: {
    type: Date,
    default: Date.now,
  },
});

BookSchema.pre('save', function (next) {
  this.slug = slugify(this.name, { lower: true });
  next();
});

module.exports = mongoose.model('Book', BookSchema);
