const fs = require('fs');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const colors = require('colors');

// Load ENV vars
dotenv.config({ path: './config/config.env' });

// Load dataBase
const URI = process.env.MONGODB_URI || 'mongodb://localhost:27017/Librarium';
mongoose.connect(URI, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});

// Load models
const Book = require('./models/Book');

// Load FIles
const books = JSON.parse(
  fs.readFileSync(`${__dirname}/_data/books.json`, 'utf-8')
);

// Import data
const importData = async () => {
  try {
    await Book.create(books);
    console.log('Data Imported...'.green.inverse);
    process.exit();
  } catch (err) {
    console.log(err);
  }
};

// Delete data
const deleteData = async () => {
  try {
    await Book.deleteMany();
    console.log('Data Destroyed...'.red.inverse);
    process.exit();
  } catch (err) {
    console.log(err);
  }
};

if (process.argv[2] === '-i') {
  importData();
} else if (process.argv[2] === '-d') {
  deleteData();
}
