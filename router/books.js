const express = require('express');

const {
  getBook,
  getBooks,
  postBook,
  putBook,
  deleteBook,
} = require('../controllers/books');

const router = express.Router();

router.route('/').get(getBooks).post(postBook);

router.route('/:id').get(getBook).put(putBook).delete(deleteBook);

module.exports = router;
