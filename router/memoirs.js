const express = require('express');

const {
  getmemoir,
  getmemoirs,
  postmemoir,
  putmemoir,
  deletememoir,
} = require('../controllers/memoirs');

const router = express.Router();

router.route('/').get(getmemoirs).post(postmemoir);

router.route('/:id').get(getmemoir).put(putmemoir).delete(deletememoir);

module.exports = router;
