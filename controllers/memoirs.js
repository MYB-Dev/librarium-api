const Memoir = require('../models/memoir');
const asyncHandler = require('../middleware/async');
const ErrorResponse = require('../utils/errorResponse');

// @desc        Get all memoirs
// @router      Get /api/v1/memoirs
// @access      Public
exports.getmemoirs = asyncHandler(async (req, res, next) => {
  const memoir = await Memoir.find();
  res.status(200).json({
    success: true,
    count: memoir.length,
    data: memoir,
  });
});

// @desc        Get a memoir
// @router      Get /api/v1/memoirs/:id
// @access      Public
exports.getmemoir = asyncHandler(async (req, res, next) => {
  const memoir = await Memoir.findById(req.params.id);

  if (!memoir)
    return next(
      new ErrorResponse(`Resource not found with ID of ${req.params.id}`, 404)
    );

  res.status(200).json({
    success: true,
    data: memoir,
  });
});

// @desc        Create a memoir
// @router      Post /api/v1/memoirs
// @access      Private
exports.postmemoir = asyncHandler(async (req, res, next) => {
  const memoir = await Memoir.create(req.body);
  res.status(201).json({ success: true, data: memoir });
});

// @desc        Update a memoir
// @router      Put /api/v1/memoirs/:id
// @access      Private
exports.putmemoir = asyncHandler(async (req, res, next) => {
  const memoir = await Memoir.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });

  if (!memoir)
    return next(
      new ErrorResponse(`Resource not found with ID of ${req.params.id}`, 404)
    );

  res.status(200).json({
    success: true,
    data: memoir,
  });
});

// @desc        Delete a memoir
// @router      Delete /api/v1/memoirs/:id
// @access      Private
exports.deletememoir = asyncHandler(async (req, res, next) => {
  const memoir = await Memoir.findByIdAndDelete(req.params.id);

  if (!memoir)
    return next(
      new ErrorResponse(`Resource not found with ID of ${req.params.id}`, 404)
    );

  res.status(200).json({ success: true, data: {} });
});
