const Book = require('../models/Book');
const asyncHandler = require('../middleware/async');
const ErrorResponse = require('../utils/errorResponse');
const { Query } = require('mongoose');

// @desc        Get all books
// @router      Get /api/v1/books
// @access      Public
exports.getBooks = asyncHandler(async (req, res, next) => {
  let query;
  // Copy req.query
  const reqQuery = { ...req.query };

  // Fields to exclude
  const rmFields = ['select', 'sort', 'page', 'limit'];

  // Loop over rmFields & delete them from reqQuery
  rmFields.forEach((field) => delete reqQuery[field]);

  // Create query string
  let queryStr = JSON.stringify(reqQuery);

  // Create Operators
  queryStr = queryStr.replace(
    /\b(gt|gte|lt|lte|in|and|or)\b/g,
    (match) => `$${match}`
  );

  // Finding Resources
  query = Book.find(JSON.parse(queryStr));

  // Select query
  if (req.query.select) {
    const fields = req.query.select.split(',').join(' ');
    query = query.select(fields);
  } else {
    query = query.select('-addedAt');
  }

  // Sort query
  if (req.query.sort) {
    const fields = req.query.sort.split(',').join(' ');
    query = query.sort(fields);
  }

  // Pagination
  const page = parseInt(req.query.page, 10) || 1;
  const limit = parseInt(req.query.limit, 10) || 25;
  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;
  const total = await Book.countDocuments();

  query = query.skip(startIndex).limit(limit);

  const pagination = {};

  if (endIndex < total) {
    pagination.next = {
      page: page + 1,
      limit,
    };
  }

  if (startIndex > 0) {
    pagination.prev = {
      page: page - 1,
      limit,
    };
  }

  // Executing query
  const book = await query;
  res.status(200).json({
    success: true,
    count: book.length,
    pagination,
    data: book,
  });
});

// @desc        Get a book
// @router      Get /api/v1/books/:id
// @access      Public
exports.getBook = asyncHandler(async (req, res, next) => {
  const book = await Book.findById(req.params.id);

  if (!book)
    return next(
      new ErrorResponse(`Resource not found with ID of ${req.params.id}`, 404)
    );

  res.status(200).json({ success: true, data: book });
});

// @desc        Create a book
// @router      Post /api/v1/books
// @access      Private
exports.postBook = asyncHandler(async (req, res, next) => {
  const book = await Book.create(req.body);
  res.status(201).json({ success: true, data: book });
});

// @desc        Update a book
// @router      Put /api/v1/books/:id
// @access      Private
exports.putBook = asyncHandler(async (req, res, next) => {
  const book = await Book.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });
  if (!book)
    return next(
      new ErrorResponse(`Resource not found with ID of ${req.params.id}`, 404)
    );

  res.status(200).json({ success: true, data: book });
});

// @desc        Delete a book
// @router      Delete /api/v1/books/:id
// @access      Private
exports.deleteBook = asyncHandler(async (req, res, next) => {
  const book = await Book.findByIdAndDelete(req.params.id);

  if (!book)
    return next(
      new ErrorResponse(`Resource not found with ID of ${req.params.id}`, 404)
    );

  res.status(200).json({ success: true, data: {} });
});
