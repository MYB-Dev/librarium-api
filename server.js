// Import Node Modules
const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const colors = require('colors');

// Import Database
const connectDB = require('./config/dataBase');
// Import Middlewares
const errorHandler = require('./middleware/error');

// Import routes
const books = require('./router/books');
const memoirs = require('./router/memoirs');

// Define App
const app = express();

// Load ENV vars
dotenv.config({ path: './config/config.env' });

// Connect to DataBase
connectDB();

// Body Parser
app.use(express.json());

// Use Mount Routes
app.use('/api/v1/books', books);
app.use('/api/v1/memoirs', memoirs);

// Use Middlewares
app.use(errorHandler);
if (process.env.NODE_ENV === 'development') app.use(morgan('dev'));

const PORT = process.env.PORT || 5000;

// Listening app
const server = app.listen(
  PORT,
  console.log(
    `Api app running in ${process.env.NODE_ENV}  listening on http://localhost:${PORT}`
      .yellow.bold
  )
);
// Unhandle promise rejection
process.on('unhandledRejection', (err, promise) => {
  console.log(`Error: ${err.message}`.red);
  // Close server & exit process
  server.close(() => process.exit(1));
});
