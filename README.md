# Librarium-api

## Discription
An RESTFUL API for the Librarium Project, with nodeJS, Express & MongoDB.
  - Authentication with Json Web Tokkens & Passport

---

### Project setup

```
npm install
```

### Run developement server

```
npm run dev
```

### Run Prodaction server

```
npm run start
```
