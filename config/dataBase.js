const mongoose = require('mongoose');

const connectDB = async () => {
  const URI = process.env.MONGODB_URI || 'mongodb://localhost:27017/Librarium';
  const connectDB = await mongoose.connect(URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  });
  console.log(
    `MongoDB Connected: ${connectDB.connection.host.underline}`.cyan.bold
  );
};

module.exports = connectDB;
